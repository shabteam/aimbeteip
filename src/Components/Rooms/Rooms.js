import React, { Component } from 'react'
import qs from 'query-string'

import './Rooms.css'

import PronosLive from '../PronosLive/PronosLive'

const baseUrl = 'https://secure-sierra-71157.herokuapp.com'
const roomsNav = {
  ROOMS: 'ROOMS',
  LIVE: 'LIVE'
}

class Rooms extends Component {
  constructor(props) {
    super(props)
    this.state = {
      roomsNav: roomsNav.ROOMS,
      rooms: [],
      roomSelected: null,
      error: false
    }
  }

  componentDidMount() {
    window.history.pushState('pageRooms', 'Title', `/rooms`)
    fetch(`${baseUrl}/api/rooms/list`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      // console.warn('GET ALL USER SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        this.setState({ error: true })
        return
      }
      this.setState({ rooms: json.rooms })
      // dispatch(getAllSuccess(json.ticket))
    })
    .catch(err => {
      console.warn('GET ALL ROOM ERROR : ', err)
      this.setState({ error: true })
    })
  }

  selectRoom = (id) => {
    window.history.pushState('pageRoom', 'Title', `/pronolive/${id}`)
    // window.history.pushState('page2', 'Title', '/page2.php');
    this.setState({ roomSelected: this.state.rooms.find(room => room.room_id === id), roomsNav: roomsNav.LIVE})
  }

  roomsNavigateTo = (nav) => {
    this.setState({ roomsNav: nav })
  }

  render() {
    const { roomIdSelected } = this.state

    return this.state.roomsNav === roomsNav.ROOMS ? (
      <div className="Rooms">
        <h3 style={{color:'white'}}>EN COURS DE DIFFUSION</h3>
        <div className="Rooms__List">
          {
            this.state.rooms.map((room, index) => (
              <div key={index} onClick={() => this.selectRoom(room.room_id)} className="Rooms__List__Item">
                <div className="Rooms__List__Item__Content">
                  <h3 style={{color:'white'}}>{room.url}</h3>
                  <h3 style={{color:'white'}}>{room.room_name}</h3>
                </div>
              </div>
            ))
          }
        </div>
      </div>
    ) : <PronosLive roomsNavigateTo={ this.roomsNavigateTo } roomSelected={this.state.roomSelected}/>
  }
}

export default Rooms

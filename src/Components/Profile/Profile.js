import React, { Component } from 'react'
import qs from 'query-string'
import { Doughnut, Pie } from 'react-chartjs-2';

import './Profile.css'

const baseUrl = 'https://secure-sierra-71157.herokuapp.com'

const dataPronoWinLoose = (right, wrong, text) => ({
	labels: [
		text + " right pronolive results", // exemple pour pronolive
		text + " wrong pronolive results",
	],
	datasets: [{
		data: [right, wrong],
		backgroundColor: [
		'#00B04D',
		'rgb(198, 23, 23)'
		],
		hoverBackgroundColor: [
		'#038039',
		'rgb(153, 15, 15)'
		]
	}]
})

const data1 = {
	labels: [
		'Red',
		'Green',
		'Yellow'
	],
	datasets: [{
		data: [300, 50, 100],
		backgroundColor: [
		'#FF6384',
		'#36A2EB',
		'#FFCE56'
		],
		hoverBackgroundColor: [
		'#FF6384',
		'#36A2EB',
		'#FFCE56'
		]
	}]
}

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataPronoWinLooseUser: dataPronoWinLoose(0,0, 'User'),
			dataPronoWinLooseGlobal: dataPronoWinLoose(0, 0, 'All users')
    }
  }

  async componentDidMount() {
		window.history.pushState('pageProfile', 'Title', `/profile`)
    let user = JSON.parse(await sessionStorage.getItem('user'));
		console.log('SESSION storage: ', sessionStorage);
		console.log('SESSION non await', sessionStorage.getItem('user'));
		console.log('USER ::: ', user.username);
    fetch(
      `${baseUrl}/api/statistics/pronolive/${user.username}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }
    )
    .then(res => res.json())
    .then(json => {
      console.log('GET PRONOLIVE STATS SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        return
      }
      this.setState({dataPronoWinLooseUser: dataPronoWinLoose(json.stats.won, json.stats.loose, 'User')}) //{...this.state.dataPronoWinLoose, datasets: [{data: json.stats}]}})
    })
    .catch(err => {
      console.log('GET LIVE FORECAST ERROR : ', err)
      this.setState({ error: err })
    })


		fetch(
      `${baseUrl}/api/statistics/pronolive`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }
    )
    .then(res => res.json())
    .then(json => {
      console.log('GLOBAL GET PRONOLIVE STATS SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        return
      }
      this.setState({dataPronoWinLooseGlobal: dataPronoWinLoose(json.stats.won, json.stats.loose, 'All users')}) //{...this.state.dataPronoWinLoose, datasets: [{data: json.stats}]}})
    })
    .catch(err => {
      console.log('GLOBAL GET LIVE FORECAST ERROR : ', err)
      this.setState({ error: err })
    })
    // let ctx = document.getElementById("chart-area").getContext("2d")
    // this.setState({myPie: new Chart(ctx, config)})
  }

  render() {
    return (
      <div className="Profile">
        <div className="Profile__Stats" style={{width:"100%"}}>
          <div className="Profile__Stats__Item">
            <Doughnut data={this.state.dataPronoWinLooseUser} />
            <h3 className="Profile__Stats__Title">Pronostics Gagnés/Perdus</h3>
          </div>
					<div className="Profile__Stats__Item">
						<Doughnut data={this.state.dataPronoWinLooseGlobal} />
						<h3 className="Profile__Stats__Title">Pronostics Gagnés/Perdus globaux.</h3>
					</div>
          {/* <div className="Profile__Stats__Item">
            <Pie data={data1} />
            <h3 className="Profile__Stats__Title">Pronostics Gagnés/Perdus</h3>
          </div>
          <div className="Profile__Stats__Item">
            <Pie data={data1} />
            <h3 className="Profile__Stats__Title">Pronostics Gagnés/Perdus</h3>
          </div>
          <div className="Profile__Stats__Item">
            <Pie data={data1} />
            <h3 className="Profile__Stats__Title">Pronostics Gagnés/Perdus</h3>
          </div> */}
        </div>
      </div>
    )
  }
}


export default Profile

import React, { Component } from 'react'
import qs from 'query-string'

import UserProfile from './UserProfile'

import './Ranking.css'

const baseUrl = 'https://secure-sierra-71157.herokuapp.com'

class Ranking extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      error: false
    }
  }

  componentDidMount() {
    window.history.pushState('pageRanking', 'Title', `/ranking`)
    fetch(`${baseUrl}/api/ranking`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
    .then(res => res.json())
    .then(json => {
      console.warn('GET ALL USER SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        this.setState({ error: true })
        return
      }
      console.log('USERS:', json.ticket)
      this.setState({ users: json.ticket })
      // dispatch(getAllSuccess(json.ticket))
    })
    .catch(err => {
      console.warn('GET ALL USER ERROR : ', err)
      this.setState({ error: true })
    })
  }

  render() {
    return (
      <div className="Ranking">
        <div className="Ranking__Results__Ranking">
          {
            this.state.users.map((profile, index) => {
                return (
                  <UserProfile
                    key={ index }
                    index={ index }
                    profile={ profile || [] }
                    style={ {marginBottom:"10px"} }/>
                )
            })
          }
        </div>
      </div>
    )
  }
}

export default Ranking

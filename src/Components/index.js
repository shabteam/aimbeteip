import React, { Component } from 'react'

import Header from './Header/Header'
import Profile from './Profile/Profile'
import Ranking from './Ranking/Ranking'
import Rooms from './Rooms/Rooms'
import PronosLive from './PronosLive/PronosLive'

const navConstants = {
  PROFILE: 'PROFILE',
  LIVE: 'LIVE',
  RANKING: 'RANKING'
}

class AppContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      menuNav: navConstants.PROFILE
    }
  }

  componentDidMount() {
  }

  menuNavigateTo = (nav) => {
    this.setState({ menuNav: nav })
  }

  renderContent = () => {
    let content = []
    let menuNav = this.state.menuNav

    content.push(<Header menuNavigateTo={ this.menuNavigateTo } navigateTo={ this.props.navigateTo } menuNav={menuNav} profile={ JSON.parse(sessionStorage.getItem('user')) } setUsername={ () => {} }/>)
    if (menuNav === navConstants.PROFILE)
      content.push(<Profile key={1}/>)
    else if (menuNav === navConstants.LIVE)
      content.push(<Rooms key={2}/>)// content.push(<PronosLive username={ 'MY USERNAME' }/>)
    else if (menuNav === navConstants.RANKING)
      content.push(<Ranking key={3}/>)
    return content
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        { this.renderContent() }
      </div>
    )
  }
}

export default AppContent

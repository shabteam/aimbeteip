import React, { Component } from 'react'

import qs from 'query-string'

import './Authentication.css'

const baseUrl = "https://secure-sierra-71157.herokuapp.com";

class Authentication extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authStep: 'SIGNIN',
      email: '',
      username: '',
      password: '',
      confirmPassword: '',
      rank:0,
      played:0,
      ratio:0,
      error: null
    }
  }

  componentDidMount() {
    window.history.pushState('pageWelcome', 'Title', `/welcome`)
  }

  onSignIn = (e) => {
    e.preventDefault()
    console.log('IN SIGN IN')
    const { username, password } = this.state
    const { navigateTo } = this.props
    if (username.length > 0 && password.length > 0)
      fetch(
        `${baseUrl}/auth/login`,
        {
          method: 'POST',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
          body: qs.stringify({ username, password })
        }
      )
      .then(res => res.json())
      .then(json => {
        if (json.status_code < 200 || json.status_code >= 400) {
          console.log('ERR signin', json.message);
          this.setState({ error: "Nom d'utilisateur ou mot de passe incorrects."})
          return
        }
        console.log(json.user.username);
        sessionStorage.setItem("user", JSON.stringify(json.user));
        //this.getRanking(json.user.username).then((info) => { console.log('INFOOO'); sessionStorage.setItem("user", JSON.stringify({...json.user, ...info})) }).catch(err => console.log('ERRRR', err));
        navigateTo('AUTHENTICATED_SCREENS')
      })
      .catch(err => {
        console.warn('USER LOGIN ERROR : ', err)
        this.setState({ error: "Une erreur est survenue. Veuillez réessayer."})
      })
    else
      this.setState({ error: "Nom d'utilisateur ou mot de passe incorrects."})
  }

  onSignUp = (e) => {
    e.preventDefault()
    console.log('IN SIGN IN')
    const { email, username, password, confirmPassword } = this.state
    const { navigateTo } = this.props
    if (email.length > 0 && username.length > 0 && (password.length > 0 && confirmPassword === password))
      fetch(
        `${baseUrl}/auth/signup`,
        {
          method: 'POST',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
          body: qs.stringify({ email, username, password })
        }
      )
      .then(res => res.json())
      .then(json => {
        if (json.status_code < 200 || json.status_code >= 400) {
          this.setState({ error: json.message})
          return
        }
        sessionStorage.setItem("user", JSON.stringify(json.user));
        navigateTo('AUTHENTIFIED_SCREENS')
      })
      .catch(err => {
        console.warn('USER SIGNUP ERROR : ', err)
        this.setState({ error: err.message})
      })
    else
      this.setState({ error: "Merci de remplir tous les champs correctement."})
  }

  goTo = (nav) => {
    this.setState({email:'', username: '', password: '', error: null, authStep: nav})
  }

  render() {
    if (this.state.authStep === 'SIGNIN')
      return (
        <div className="Auth">
          <div className="Auth__Logo"></div>
          <form onSubmit={this.onSignIn} className="Auth__Form" style={{}}>
              <input onChange={(e) => this.setState({ username: e.target.value })} className="Auth__Input" placeholder="Nom d'utilisateur" type="text" name="username" value={this.state.username} required/>
              <input onChange={(e) => this.setState({ password: e.target.value })} className="Auth__Input" placeholder="Mot de passe" type="password" name="password" value={this.state.password} required/>
              <input className="Auth__Button" type="submit" value="Se connecter"/>
          </form>
          {
            this.state.error &&
            <div className="Auth__Error">
              { this.state.error }
            </div>
          }
          <div style={{textAlign:'center'}}>
            <span>
              Vous n'avez pas de compte ?&nbsp;
              <a href="#" onClick={() => this.goTo('SIGNUP')} className="Auth__Link">Inscrivez-vous</a>
            </span>
          </div>
        </div>
      )
    else
      return (
        <div className="Auth">
          <div className="Auth__Logo"></div>
          <form onSubmit={this.onSignUp} className="Auth__Form" style={{}}>
              <input onChange={(e) => this.setState({ email: e.target.value })} className="Auth__Input" placeholder="Adresse email" type="email" name="email" value={this.state.email} required/>
              <input onChange={(e) => this.setState({ username: e.target.value })} className="Auth__Input" placeholder="Nom d'utilisateur" type="text" name="username" value={this.state.username} required/>
              <input onChange={(e) => this.setState({ password: e.target.value })} className="Auth__Input" placeholder="Mot de passe" type="password" name="password" value={this.state.password} required/>
              <input onChange={(e) => this.setState({ confirmPassword: e.target.value })} className="Auth__Input" placeholder="Retaper mot de passe" type="password" name="confirmPassword" value={this.state.confirmPassword} required/>
              <input className="Auth__Button" type="submit" value="Créer un compte"/>
          </form>
          {
            this.state.error &&
            <div className="Auth__Error">
              { this.state.error }
            </div>
          }
          <div style={{textAlign:'center'}}>
            <span>
              Vous avez déjà un compte ?&nbsp;
              <a href="#" onClick={() => this.goTo('SIGNIN')} className="Auth__Link">Connectez-vous</a>
            </span>
          </div>
        </div>
      )
  }
}

export default Authentication

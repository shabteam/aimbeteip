import React, { Component } from 'react'
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// import Header from './Components/Header/Header'
// import PronosLive from './Components/PronosLive/PronosLive'
import qs from 'query-string'

// import './App.css'

const baseUrl = "https://secure-sierra-71157.herokuapp.com";

class Authentication extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _id: '',
      username: sessionStorage.getItem("username") === null ? '' : sessionStorage.getItem("username"),
      password: '',
      email: sessionStorage.getItem("email") === null ? '' : sessionStorage.getItem("email"),
      authenticated: sessionStorage.getItem("authenticated") === null ? false : sessionStorage.getItem("authenticated"),
      token: sessionStorage.getItem("token") === null ? '' : sessionStorage.getItem("token"),
      redgate: sessionStorage.getItem("redgate") === null ? '' : sessionStorage.getItem("redgate"),
      points: sessionStorage.getItem("points") === null ? 0 : sessionStorage.getItem("points"),
      matchs_won: sessionStorage.getItem("matchs_won") === null ? 0 : sessionStorage.getItem("matchs_won"),
      matchs_lose: sessionStorage.getItem("matchs_lose") === null ? 0 : sessionStorage.getItem("matchs_lose")
    }
  }

  onUsernameChange(e) {
    this.setState({username: e.target.value});
  }

  onPasswordChange(e) {
    this.setState({password: e.target.value});
  }

  onSubmit(e) {
    var qs = require('qs');
    var username = this.state.username;
    var password = this.state.password;
    if (this.state.username === '' || this.state.password === '')
    {
       this.setState({ errorMessage: 'Veuillez renseigner le nom d\'utilisateur et le mot de passe.'  });
    }

    else {
      //this.setState({authenticated: true});
      fetch(
        `${baseUrl}/auth/login`,
        {
          method: 'POST',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
          body: qs.stringify({ username, password })
        }
      )
      .then(res => res.json())
      .then(json => {
        if (json.status_code < 200 || json.status_code >= 400) {
          this.setState({ errorMessage: 'Invalid user name or password.' });
          console.log(json);
          return;
        }
        // console.log(json);
        // this.setState({ _id: json["_id"]["$oid"] });
        // this.setState({ token: json["access_token"] });
        // sessionStorage.setItem("token", this.state.token);
        // console.log(this.state.token);
        // this.setState({ redgate: json["redgate"] });
        // sessionStorage.setItem("redgate", this.state.redgate);
        // console.log(this.state.redgate);
        // this.setState({ points: json["points"] });
        // sessionStorage.setItem("points", this.state.points);
        // console.log(this.state.points);
        // this.setState({ matchs_won: json["matchs_won"] });
        // sessionStorage.setItem("matchs_won", this.state.matchs_won);
        // console.log(this.state.matchs_won);
        // this.setState({ matchs_lose: json["matchs_loose"] });
        // sessionStorage.setItem("matchs_lose", this.state.matchs_lose);
        // console.log(this.state.matchs_lose);
        this.setState({ authenticated: true });
        sessionStorage.setItem("authenticated", true);
        console.log(this.state.authenticated);
        // console.log(sessionStorage.getItem("username"));
        // console.log(sessionStorage.getItem("token"));
        // console.log(sessionStorage.getItem("authenticated"));
        // console.log(sessionStorage.getItem("no"));
        // console.log(sessionStorage.getItem("points"));
        // console.log(sessionStorage.getItem("matchs_won"));
        // console.log(sessionStorage.getItem("matchs_lose"));
      })
      .catch(err => {
        console.warn('USER LOGIN ERROR : ', err)
      })
    }
  }

  setUsername = (username) => {
    this.setState({ username })
  }

  render() {
    var isAuth = sessionStorage.getItem("authenticated") === null ? this.state.authenticated : sessionStorage.getItem("authenticated");
    const style = {
        margin: 20,
        color: "black"
    };
    return (
      <div  className="App" style={{color : 'black', margin:'0 auto', textAlign:'center', width: "400"}}>
      <h2 style={{color : 'white'}}>Login</h2>

      <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
      <Paper zDepth={2}>

      <label>Username : </label>
      <TextField hintText="Username" style={style} value={this.state.username}  onChange={ this.onUsernameChange.bind(this)} underlineShow={true} />
      <div className="errorMessage">{this.state.errorUserMessage}  </div>

      <Divider />
      <label>Password : </label>
      <TextField hintText="Password" type="password" style={style} value={this.state.password}  onChange={ this.onPasswordChange.bind(this)} underlineShow={true} />
      <div className="errorMessage">{this.state.errorPasswordMessage} </div>
      <Divider />
      <RaisedButton onClick={this.onSubmit.bind(this)} primary={true} value="login" label="Se connecter" style={style} />

      </Paper>
      </MuiThemeProvider>

      <br />
      <div className="errorMessage"> {this.state.errorMessage}  </div>

        </div>
    )
  }
}

export default Authentication

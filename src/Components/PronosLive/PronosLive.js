import React, { Component } from 'react'
import qs from 'query-string'

import './PronosLive.css'

import UserProfile from './UserProfile'
import UserProfileMe from './UserProfileMe'
import Forecast from './Forecast'
import ForecastManager from './ForecastManager'

// import { roomUsers } from '../../FakeApi/RoomUsers'

const baseUrl = 'https://secure-sierra-71157.herokuapp.com'

const RESULTS_MENU = {
  RANKING: 'RANKING',
  HISTORY: 'HISTORY',
  CHAT: 'CHAT'
}

const FORECAST_MENU = {
  LIVE: 'LIVE',
  PENDING: 'PENDING'
}

class PronosLive extends Component {
  constructor(props) {
    super(props)
    this.state = {
      resultsMenu: RESULTS_MENU.RANKING,
      forecastMenu: FORECAST_MENU.LIVE,
      forecast: null,
      rankingProfiles: [],
      error: null,
      rank: 1
    }

    let p = window.location.pathname.split('/');
    this.room_id =  p[p.length-1];
    this.ws = new WebSocket('wss://secure-sierra-71157.herokuapp.com/' + p[p.length-1]);
    console.log('WS: ', this.ws)
    this.ws.onopen = () => {
      console.log('WS IS OPEN : ', this.ws.readyState);
      setInterval(() => this.ws.readyState != 0 ? this.ws.send('__ping__') : null, 5000);
    }
    this.ws.onclose = () => {
      this.ws = new WebSocket('wss://secure-sierra-71157.herokuapp.com/' + p[p.length-1]);
      console.log('WS IS CLOSED : ', this.ws.readyState);
    }
    this.ws.onmessage = (msg) => {
      console.log('MSG: ', msg)
      const json = JSON.parse(msg.data)
      if (json.type === 'create')
        this.getLiveForecast(json.type, json.id)
      else if (json.type === 'valid')
        this.getLiveRanking()
    }
  }

  componentDidMount() {
    setInterval(this.getLiveRanking, 3000)
  }

  // componentDidMount() {
  //   this.ws = new WebSocket(`ws://secure-sierra-71157.herokuapp.com`)
  //   console.log('WS: ', this.ws)
  //   this.ws.onopen = () => {
  //     console.log('WS IS OPEN : ', this.ws.readyState)
  //   }
  //   this.ws.onmessage = (msg) => {
  //     console.log('MSG: ', msg)
  //     const json = JSON.parse(msg.data)
  //     if (json.type === 'create')
  //       this.getLiveForecast(json.type, json.id)
  //     else if (json.type === 'valid')
  //       this.getLiveRanking()
  //   }
  // }

  getLiveForecast = (type, forecastId) => {
    fetch(
      `${baseUrl}/api/pronostic/${forecastId}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }
    )
    .then(res => res.json())
    .then(json => {
      console.log('GET LIVE FORECAST SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        this.setState({error: json.message})
        return
      }
      this.setState({ error: null, forecast: json.pronostic/*[0]*/ })
    })
    .catch(err => {
      console.log('GET LIVE FORECAST ERROR : ', err)
      this.setState({ error: err })
    })
  }

  getLiveRanking = () => {
    fetch(
      `${baseUrl}/api/room/${this.room_id}/ranking`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }
    )
    .then(res => res.json())
    .then(json => {
      console.log('GET LIVE RANKING SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        this.setState({error: json.message})
        return
      }
      let username = JSON.parse(sessionStorage.getItem('user')).username;
      let rank = 0;
      for(let i = 0; i < json.pronostics.length; ++i)
      {
        if(json.pronostics[i].username == username)
        {
          rank = i;
          break;
        }
      }
      this.setState({ error: null, rankingProfiles: json.pronostics, rank: rank })
    })
    .catch(err => {
      console.log('GET LIVE RANKING ERROR : ', err)
      this.setState({ error: err })
    })
  }

  removeForecast = (forecastId) => {
    this.setState({ forecast: null })
  }

  selectChoice = (index, idForecast) => {
    console.log('IN SELECT CHOICE')
    console.log('ID FORECAST: ', idForecast)
    //console.log('USERNAME: ', this.props.username)
    console.log('CHOICE: ', index)
    let data = new FormData()
    let username = JSON.parse(sessionStorage.getItem('user')).username;
    data.append("json", JSON.stringify({
      choice: index,
      username: username
    }))
    fetch(
      `${baseUrl}/api/pronostic/${idForecast}/select`,
      {
        method: 'POST',
        mode:"no-cors",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: qs.stringify({
          username: username,
          choice: index
        })
      }
    )
    .then(json => {
      console.log('SELECT CHOICE  SUCCESS : ', json)
      if (json.status_code < 200 || json.status_code >= 400) {
        this.setState({error: json.message})
        return
      }
      // this.setState({ error: null, rankingProfiles: json.data })
    })
    .catch(err => {
      console.log('SELECT CHOICE ERROR : ', err)
      this.setState({ error: err })
    })
  }

  render() {
    console.log('ROOM SELECTED: ', this.props.roomSelected)
    return (
      <div className="PronosLive">
        <span
          onClick={ () => {this.props.roomsNavigateTo('ROOMS'); window.history.pushState('pageRooms', 'Title', `/rooms`)} }
          className="PronosLive__ReturnButton">
          <i className="fa fa-arrow-left"></i>
        </span>
        <div className="PronosLive__Section" style={ {flex:1} }>
          <div className="PronosLive__Stream">
            <iframe
              style={ {border:"0", width:"100%", height:"100%"} }
              src={`https://player.twitch.tv/?channel=${this.props.roomSelected.url}`}
              frameBorder="0"
              allowFullScreen="true"
              scrolling="no">
            </iframe>
          </div>
          <div className="PronosLive__Forecast">
            <div className="PronosLive__Forecast__Menu">
              <div
                onClick={ () => this.setState({ forecastMenu: FORECAST_MENU.LIVE }) }
                className={this.state.forecastMenu === FORECAST_MENU.LIVE ? "PronosLive__Forecast__Menu__Item--Selected" : "PronosLive__Forecast__Menu__Item"}>
                {
                  this.state.forecast ? (
                    <div className="PronosLive__Forecast__Menu__Item__Badge">1</div>
                  ) : null
                }
                <span>Live Pronosis</span>
              </div>
              {/* <div
                onClick={ () => this.setState({ forecastMenu: FORECAST_MENU.PENDING }) }
                className={this.state.forecastMenu === FORECAST_MENU.PENDING ? "PronosLive__Forecast__Menu__Item--Selected" : "PronosLive__Forecast__Menu__Item"}>
                <span>Pending Pronosis</span>
              </div> */}
            </div>
            {
              this.state.forecast ? (
                <ForecastManager
                  canSelectAnswer
                  selectChoice={ this.selectChoice }
                  removeForecast={ this.removeForecast }
                  forecast={ this.state.forecast }/>
              ) : (
                <div style={ {height:"100%", display:"flex", alignItems:"center", justifyContent:"center"} }>
                  <span style={ {color:"white", fontWeight:"bold", fontSize:20} }>En attente d'un nouveau pronostic...</span>
                </div>
              )
            }
            {/* {
              this.state.forecastMenu === FORECAST_MENU.LIVE ? (
                <ForecastManager canSelectAnswer forecast={ this.state.forecast || null }/>
              ) : (
                <div style={ {display:"flex", flexDirection:"row", width:"100%", height:"100%", paddingLeft:"10px", borderLeft: "1px solid #00B04D"} }>
                  <Forecast style={ {marginBottom:"10px"} }/>
                </div>
              )
            } */}
          </div>
        </div>
        <div className="PronosLive__Section" style={ {width:"425px"} }>
          {/* <UserProfile index={ 0 } profile={ this.state.rankingProfiles.find(profile => profile.username === this.props.username) }/> */}
          <div className="PronosLive__Results">
            <div className="PronosLive__Results__Menu">
              <span
                onClick={ () => this.setState({ resultsMenu: RESULTS_MENU.RANKING }) }
                className={this.state.resultsMenu === RESULTS_MENU.RANKING ? "PronosLive__Results__Menu__Item--Selected" : "PronosLive__Results__Menu__Item"}>RANKING</span>
              {/* <span
                onClick={ () => this.setState({ resultsMenu: RESULTS_MENU.HISTORY }) }
                className={this.state.resultsMenu === RESULTS_MENU.HISTORY ? "PronosLive__Results__Menu__Item--Selected" : "PronosLive__Results__Menu__Item"}>HISTORY</span> */}
              <span
                onClick={ () => this.setState({ resultsMenu: RESULTS_MENU.CHAT }) }
                className={this.state.resultsMenu === RESULTS_MENU.CHAT ? "PronosLive__Results__Menu__Item--Selected" : "PronosLive__Results__Menu__Item"}>CHAT</span>
            </div>
            <UserProfileMe
              index={ this.state.rank }
              profile={ JSON.parse(sessionStorage.getItem('user')) || [] }
              style={ {marginBottom:"10px"} }/>
            {
              this.state.resultsMenu === RESULTS_MENU.RANKING ? (
                <div className="PronosLive__Results__Ranking">
                  {
                    this.state.rankingProfiles.length <= 0 ? (
                      <span>La session n'a pas encore commencé...</span>
                    ) : null
                  }
                  {
                    this.state.rankingProfiles.map((profile, index) => {
                      if (profile)
                        return (
                          <UserProfile
                            key={ index }
                            index={ index }
                            profile={ profile || [] }
                            style={ {marginBottom:"10px"} }/>
                        )
                    })
                  }
                </div>
              ) : (this.state.resultsMenu === RESULTS_MENU.HISTORY ? (
                <div className="PronosLive__Results__History">
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                  <Forecast style={ {marginBottom:"10px"} }/>
                </div>
              ) : (
                <div className="PronosLive__Results__Chat">
                  <span style={{position:'absolute', top:0, margin:'0 auto'}}>Chargement du chat...</span>
                  <iframe
                    style={ {width:"100%", height:"100%", border:0} }
                    src={`https://www.twitch.tv/${this.props.roomSelected.url}/chat?popout=`}
                    frameBorder="0"
                    scrolling="no"></iframe>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    )
  }
}


export default PronosLive

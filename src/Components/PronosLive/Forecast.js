import React, { Component } from 'react'
import './Forecast.css'

class Forecast extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  selectChoice = (index, forecastId) => {
    this.setState({ choice: index })
    this.props.selectChoice(index, forecastId)
  }

  render() {
    const { forecast } = this.props
      return (
        <div className="PronosLive__Forecast__Item">
          <div className="PronosLive__Forecast__Data">
            <div className="PronosLive__Forecast__Question">
              <div
                style={ {backgroundImage: `url('${ 'http://lessarcasticks.com/wp-content/uploads/2016/02/sfv-logo.png' }')`} }
                className="PronosLive__Forecast__Question__Logo"></div>
              <span className="PronosLive__Forecast__Question__Text">
                <span>{ forecast.question }</span>
              </span>
            </div>
            {
              this.props.canSelectAnswer ? (
                <div className="PronosLive__Forecast__Answers">
                  {
                    forecast.choices.map((choice, index) => (
                      <span key={ index } onClick={ () => this.selectChoice(index, forecast._id) } className={ `PronosLive__Forecast__Answer${this.state.choice === index ? '--Selected' : ''}`}>
                        {choice}
                      </span>
                    ))
                  }
                </div>
              ) : (
                <div className="PronosLive__Forecast__Answers">
                  <span className="PronosLive__Forecast__Answer--Selected">Chun-Li</span>
                  <span className="PronosLive__Forecast__Answer--Blocked">Ryu</span>
                </div>
              )
            }
          </div>
        </div>
      )
  }
}

export default Forecast

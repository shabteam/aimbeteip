import React, { Component } from 'react'
import CircularProgressbar from 'react-circular-progressbar'

import Forecast from './Forecast'
import './ForecastManager.css'

class ForecastManager extends Component {
  constructor(props) {
    super(props)
    this.state = {
      secondsLeft: this.props.forecast.timeout
    }

    this.updateSecondsLeft = this.updateSecondsLeft.bind(this)

    this.timer = setInterval(this.updateSecondsLeft, 1000)
  }

  updateSecondsLeft() {
    const sl = this.state.secondsLeft
    if (sl && sl > 0)
      this.setState({ secondsLeft: sl - 1 })
    else {
      clearInterval(this.timer)
      this.props.removeForecast(this.props.forecast._id)
    }
  }

  render() {
    return (
      <div style={ {display:"flex", flexDirection:"row", width:"100%", height:"100%", paddingLeft:"10px", borderLeft: "1px solid #00B04D"} }>
        <Forecast canSelectAnswer selectChoice={ this.props.selectChoice } forecast={ this.props.forecast } style={ {marginBottom:"10px"} }/>
        <div className="PronosLive__Forecast__Countdown">
          <span>{ this.state.secondsLeft }</span>
          {/* <CircularProgressbar background percentage={60} /> */}
        </div>
      </div>
    )
  }
}

export default ForecastManager

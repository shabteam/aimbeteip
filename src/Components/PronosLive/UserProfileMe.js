import React from 'react'
import './UserProfile.css'

const UserProfileMe = (props) => {
  const { profile } = props
  console.log('PROIFLE ME:', profile)
  const totalForecast = profile.matchs_won + profile.matchs_loose
    const setRankColor = (rank) => {
      if (rank === 1)
        return { backgroundColor:'#F8D11C' }
      else if (rank === 2)
        return { backgroundColor: '#cbcbcb' }
      else if (rank === 3)
        return { backgroundColor: '#c46333' }
      return {}
    }
    return (
      <div style={ props.style } className="PronosLive__UserProfile">
        <div
          style={ {backgroundImage: "url('http://www.lovemarks.com/wp-content/uploads/profile-avatars/default-avatar-knives-ninja.png')"} }
          className="PronosLive__UserProfile__Avatar"></div>
        <div className="PronosLive__UserProfile__Infos">
          <div
            style={ setRankColor(props.index + 1) }
            className="PronosLive__UserProfile__Infos__Ranking">
            #{ props.index + 1 }
          </div>
          <div className="PronosLive__UserProfile__Infos__Username">
            <i className="fa fa-square" style={ {fontSize: "10px", color: "rgb(28, 160, 235)"} }></i>
            &nbsp;{ profile.username }
          </div>
        </div>
        <div className="PronosLive__UserProfile__Results">
          <div style={ {backgroundColor: "#00B04D"} } className="PronosLive__UserProfile__Result">
            <i className="fa fa-thumbs-up"></i>&nbsp;&nbsp;
            <span>{ profile.matchs_won }</span>
          </div>
          <div style={ {backgroundColor: "rgb(186, 24, 24)"} } className="PronosLive__UserProfile__Result">
            <i className="fa fa-thumbs-down"></i>&nbsp;&nbsp;
            <span>{ profile.matchs_loose }</span>
          </div>
          <div style={ {backgroundColor: "#F68D23"} } className="PronosLive__UserProfile__Result">
            <i className="fa fa-line-chart"></i>&nbsp;&nbsp;
            <span>{ profile.matchs_won > 0 ? (profile.matchs_won / (profile.matchs_won + profile.matchs_loose) * 100).toFixed(2) : 0 }%</span>
          </div>
        </div>
      </div>
    )
}

export default UserProfileMe

import React, { Component } from 'react';

import './Header.css'
const baseUrl = "https://secure-sierra-71157.herokuapp.com";

class Header extends Component {
  state = {
    rank:0,
    played:0,
    ratio:0
  }
  getRanking = (username) => {
    if(username.length > 0) {
      return fetch(`${baseUrl}/api/ranking`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        }
      })
      .then(res => res.json())
      .then(json => {
        let rank = 0;
        for(let i = 0; i < json.ticket.length; ++i)
        {
          if(json.ticket[i].username == username)
          {
            rank = i + 1;
            break;
          }
        }
        console.log('RANK ', rank);
        fetch(`${baseUrl}/api/statistics/pronolive/${username}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
        .then(res1 => { return res1.json()})
        .then(json2 => { console.log('IN JSON'); this.setState({rank, played: (json2.win + json2.loose) || 0, ratio : (json2.win + json2.loose) == 0 ? 0 : ((json2.win) / (json2.win + json2.loose)) }) })
        // console.warn('GET ALL USER SUCCESS : ', json)
        // if (json.status_code < 200 || json.status_code >= 400) {
        //   this.setState({ error: true })
        //   return
        // }
        // console.log('USERS:', json.ticket)
        // this.setState({ users: json.ticket })
        // dispatch(getAllSuccess(json.ticket))
      })
      .catch(err => {
        // console.warn('GET ALL USER ERROR : ', err)
        // this.setState({ error: true })
      })
    }
  }

  componentDidMount = () => {
     this.getRanking(this.props.profile.username);
    setInterval(function() { this.getRanking(this.props.profile.username)}.bind(this), 2000);
  }
  render = () => {
    const props = this.props;
    console.log('RENDER Header');
      return (
        <div className="Header">
          <div onClick={ () => props.menuNavigateTo('PROFILE') } className="Header__Profile">
            <div className="Header__Profile__Avatar"></div>
            <div className="Header__Profile__Info">
              <div className="Header__Profile__Info__Item">
                <div
                  className="Header__Profile__Info__Item__Rank"
                  style={ {backgroundColor: "#F8D11C"} }>#{this.state.rank}</div>
                <div className="Header__Profile__Info__Item__Username">
                  {/* <i className="fa fa-square" style={ {fontSize: "10px", color: "rgb(28, 160, 235)"} }></i>&nbsp; */}
                  { props.profile.username }
                  {/* <input
                    style={ {height:"100%", backgroundColor:'transparent', outline:'none', border:'0', color:'white', fontWeight:'bold'} }
                    type="text"
                    value={props.username}
                    onChange={ (e) => props.setUsername(e.target.value) }/> */}
                </div>
              </div>
              <div className="Header__Profile__Info__Item">
                <div style={ {backgroundColor: "#00B04D"} } className="Header__Profile__Info__Item__Counter">
                  <i className="fa fa-ticket"></i>&nbsp;&nbsp;{ this.state.played /*props.profile.matchs_won + props.profile.matchs_loose*/ }
                </div>
                <div style={ {backgroundColor: "#F68D23"} } className="Header__Profile__Info__Item__Counter">
                  <i className="fa fa-line-chart"></i>&nbsp;&nbsp;
                  { props.profile.matchs_won > 0 ? (this.state.ratio * 100).toFixed(2) : 0 }%
                </div>
              </div>
              {/* <div
                className="Header__Profile__Info__Item"
                style={ {backgroundColor: "white", color: "#303030"} }>
                <i className="fa fa-diamond" style={ {color: "rgb(122, 207, 255)"} }></i>&nbsp;&nbsp;Diamond League
              </div> */}
            </div>
          </div>
          <div className="Header__Buttons">
            {/* <a href="#"><i className="fa fa-cog"></i></a> */}
            <a onClick={ async () => {
              console.log('CLICK ON DISCONNECT')
              await sessionStorage.removeItem('user')
              props.navigateTo('AUTHENTICATION')
            } } style={ {backgroundColor: "rgb(186, 24, 24)"} }><i className="fa fa-power-off"></i></a>
          </div>
          <div className="Header__Nav">
            {/* <a href="#">
              <div className="Header__Nav__Icon"><i className="fa fa-home"></i></div>
              <div className="Header__Nav__Title">HOME</div>
            </a>
            <a href="#">
              <div className="Header__Nav__Icon"><i className="fa fa-gamepad"></i></div>
              <div className="Header__Nav__Title">MATCHS</div>
            </a> */}
            <a href="#" style={{backgroundColor: props.menuNav === 'LIVE' ? '#00B04D' : ''}} onClick={ () => props.menuNavigateTo('LIVE') }>
              <div className="Header__Nav__Icon"><i className="fa fa-video-camera"></i></div>
              <div className="Header__Nav__Title"><i className="fa fa-circle" style={ {fontSize: "9px", color: "rgb(186, 24, 24)"} }></i>&nbsp;STREAMS</div>
            </a>
            <a href="#" style={{backgroundColor: props.menuNav === 'RANKING' ? '#00B04D' : ''}} onClick={ () => props.menuNavigateTo('RANKING') }>
              <div className="Header__Nav__Icon"><i className="fa fa-trophy"></i></div>
              <div className="Header__Nav__Title">RANKING</div>
            </a>
          </div>
        </div>
      )
  }

}
//
// const Header = (props) => {
//     return (
//       <div className="Header">
//         <div onClick={ () => props.menuNavigateTo('PROFILE') } className="Header__Profile">
//           <div className="Header__Profile__Avatar"></div>
//           <div className="Header__Profile__Info">
//             <div className="Header__Profile__Info__Item">
//               <div
//                 className="Header__Profile__Info__Item__Rank"
//                 style={ {backgroundColor: "#F8D11C"} }>#0</div>
//               <div className="Header__Profile__Info__Item__Username">
//                 {/* <i className="fa fa-square" style={ {fontSize: "10px", color: "rgb(28, 160, 235)"} }></i>&nbsp; */}
//                 { props.profile.username }
//                 {/* <input
//                   style={ {height:"100%", backgroundColor:'transparent', outline:'none', border:'0', color:'white', fontWeight:'bold'} }
//                   type="text"
//                   value={props.username}
//                   onChange={ (e) => props.setUsername(e.target.value) }/> */}
//               </div>
//             </div>
//             <div className="Header__Profile__Info__Item">
//               <div style={ {backgroundColor: "#00B04D"} } className="Header__Profile__Info__Item__Counter">
//                 <i className="fa fa-ticket"></i>&nbsp;&nbsp;{ props.profile.matchs_won + props.profile.matchs_loose }
//               </div>
//               <div style={ {backgroundColor: "#F68D23"} } className="Header__Profile__Info__Item__Counter">
//                 <i className="fa fa-line-chart"></i>&nbsp;&nbsp;
//                 { props.profile.matchs_won > 0 ? (props.profile.matchs_won / (props.profile.matchs_won + props.profile.matchs_loose) * 100).toFixed(2) : 0 }%
//               </div>
//             </div>
//             {/* <div
//               className="Header__Profile__Info__Item"
//               style={ {backgroundColor: "white", color: "#303030"} }>
//               <i className="fa fa-diamond" style={ {color: "rgb(122, 207, 255)"} }></i>&nbsp;&nbsp;Diamond League
//             </div> */}
//           </div>
//         </div>
//         <div className="Header__Buttons">
//           <a href="#"><i className="fa fa-cog"></i></a>
//           <a href="#" onClick={ async () => {
//             console.log('CLICK ON DISCONNECT')
//             await sessionStorage.removeItem('user')
//             props.navigateTo('AUTHENTICATION')
//           } } style={ {backgroundColor: "rgb(186, 24, 24)"} }><i className="fa fa-power-off"></i></a>
//         </div>
//         <div className="Header__Nav">
//           {/* <a href="#">
//             <div className="Header__Nav__Icon"><i className="fa fa-home"></i></div>
//             <div className="Header__Nav__Title">HOME</div>
//           </a>
//           <a href="#">
//             <div className="Header__Nav__Icon"><i className="fa fa-gamepad"></i></div>
//             <div className="Header__Nav__Title">MATCHS</div>
//           </a> */}
//           <a href="#" style={{backgroundColor: props.menuNav === 'LIVE' ? '#00B04D' : ''}} onClick={ () => props.menuNavigateTo('LIVE') }>
//             <div className="Header__Nav__Icon"><i className="fa fa-video-camera"></i></div>
//             <div className="Header__Nav__Title"><i className="fa fa-circle" style={ {fontSize: "9px", color: "rgb(186, 24, 24)"} }></i>&nbsp;STREAMS</div>
//           </a>
//           <a href="#" style={{backgroundColor: props.menuNav === 'RANKING' ? '#00B04D' : ''}} onClick={ () => props.menuNavigateTo('RANKING') }>
//             <div className="Header__Nav__Icon"><i className="fa fa-trophy"></i></div>
//             <div className="Header__Nav__Title">RANKING</div>
//           </a>
//         </div>
//       </div>
//     )
// }

export default Header

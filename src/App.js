import React, { Component } from 'react'

import Authentication from './Components/Authentication/Authentication'
import Header from './Components/Header/Header'
import PronosLive from './Components/PronosLive/PronosLive'
import AppContent from './Components/index'


import './App.css'

 const nav = {
   AUTHENTICATION: 'AUTHENTICATION',
   AUTHENTICATED_SCREENS: 'AUTHENTICATED_SCREENS'
 }

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nav: nav.AUTHENTICATION
    }
  }

  componentDidMount() {
    let isAuth = sessionStorage.getItem("user")
    if (isAuth)
      this.navigateTo(nav.AUTHENTICATED_SCREENS)
  }

  navigateTo = (nav) => {
    this.setState({ nav })
  }

  render() {
    return (
      <div className="App">
        {
          this.state.nav === nav.AUTHENTICATION ? (
            <Authentication navigateTo={this.navigateTo}/>
          ) : (
            <AppContent navigateTo={this.navigateTo}/>
            // <div style={ {height:"100%"} }>
            //   <Header navigateTo={ this.navigateTo } username={ this.state.username } setUsername={ this.setUsername }/>
            //   <PronosLive username={ this.state.username }/>
            // </div>
          )
        }
      </div>
    )
  }
}

export default App

export const roomUsers = () => {
  return [
    {
      id:0,
      avatar_url: 'http://www.snut.fr/wp-content/uploads/2015/07/image-de-hulk.jpg',
      username:'ImAtome',
      gender:0,
      rank: 1,
      good: 23,
      bad: 5
    },
    {
      id:1,
      avatar_url: 'https://capelesscrusader.org/wp-content/uploads/2017/08/Matt-Reeves-The-Batman.jpg',
      username:'Batman',
      gender:0,
      rank: 2,
      good: 17,
      bad: 6
    },
    {
      id:2,
      avatar_url: 'https://www.sideshowtoy.com/photo_902622_thumb.jpg',
      username:'Ironman',
      gender:0,
      rank: 3,
      good: 12,
      bad: 6
    },
    {
      id:3,
      avatar_url: 'http://junkee.com/wp-content/uploads/2017/06/Wonder-Woman-Movie-Sexism-Feminism.jpg',
      username:'Wondermom',
      gender:1,
      rank: 4,
      good: 10,
      bad: 5
    }
  ]
}
